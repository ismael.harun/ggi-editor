# ggi-editor

A GNU graphic image editor with script and pipeline features.

Currently in the planning stages, ggi-editor will support thr following features:

 1) multilayer, and sub groups and folders
 2) multiple layer pixel types
 3) multiple vector and point types
 4) view and rendered view caching
 5) plugin and binding support
 6) real-time script CLI
 7) remote client pipelining
 8) multiple renderer support
 9) importing and exporting of popular graphic image formats
10) Linux and Andoid platforms



